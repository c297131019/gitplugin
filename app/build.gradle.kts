import com.chenchen.plugin.git.*

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    //添加插件
    id("GitPlugin")
}

android {
    compileSdk = 31
    defaultConfig {
        applicationId = "com.chenchen.gitplugin"
        minSdk = 23
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        release {
//            minifyEnabled false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation("androidx.core:core-ktx:1.7.0")
    implementation("androidx.appcompat:appcompat:1.4.1")
    implementation("com.google.android.material:material:1.5.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.3")
}

gitClone {
    //很多项目的远程仓库不止一个，这里可以设置仓库的域名
    gitStore("https://gitee.com") {
        //配置仓库中的项目
        gitProject(
            //项目链接
            url = "jaso_chen.com/camera-study",
            //项目分支，以后切换分支大概就是修改branch
            branch = "master",
            //存放路径
            saveDir = buildDir.absolutePath + "/testDir",
            //是否需要重命名文件夹
            rename = "CameraStudy")
        gitProject(
            url = "jaso_chen.com/camera-study",
            branch = "testBranch",
            saveDir = buildDir.absolutePath + "/testDir",
            rename = "CameraStudyTest")
    }
}