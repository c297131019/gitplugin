package com.chenchen.plugin.git

import java.io.BufferedReader
import java.io.File
import java.io.InputStream
import java.io.InputStreamReader

/**
 * 全局可执行的命令
 */
internal fun String.exeCommand(): String {
    println("exeCommand --------> $this")
    var process: Process? = null
    try {
        process = Runtime.getRuntime().exec(this)
        process?.waitFor()
    } catch (e: Exception) {
        println("$this waiting An exception occurs. ${e.message}")
    }
    val errorMsg = process?.errorStream?.getErrorMsg() ?: ""
    println("exeCommand --------> $errorMsg")
    return errorMsg
}

/**
 * 需要进入到某个目录下才能执行的命令
 * @param dir 传入目录路径
 */
internal fun String.exeCommand(dir: String): String {
    println("exeCommand --------> $this $dir")
    var process: Process? = null
    try {
        process = Runtime.getRuntime().exec(this, null, File(dir))
        process?.waitFor()
    } catch (e: Exception) {
        println("$this waiting An exception occurs. ${e.message}")
    }
    val errorMsg = process?.errorStream?.getErrorMsg() ?: ""
    println("exeCommand --------> $errorMsg")
    return errorMsg
}


/**
 * 获取错误信息
 */
internal fun InputStream.getErrorMsg() =
    BufferedReader(InputStreamReader(this)).use {
        it.readText()
    }