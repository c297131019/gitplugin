package com.chenchen.plugin.git

import com.chenchen.plugin.meta.GitProject
import com.chenchen.plugin.meta.GitScope
import com.chenchen.plugin.meta.GitStore

fun scope(scope: GitScope.() -> Unit) {
    val gitScope = GitScope()
    gitScope.scope()
}

fun GitScope.gitStore(host: String, scope: GitStore.() -> Unit) {
    val store = GitStore(host)
    store.scope()
    this.stores.add(store)
}

fun GitStore.gitProject(url: String, branch: String = "", saveDir: String, rename: String = "") {
    val project = GitProject(url, branch, saveDir, rename)
    this.projects.add(project)
}