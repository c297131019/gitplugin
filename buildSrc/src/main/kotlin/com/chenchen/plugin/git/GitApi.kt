package com.chenchen.plugin.git

import java.io.BufferedReader
import java.io.File
import java.io.InputStream
import java.io.InputStreamReader

/**
 * 发起gitClone请求
 */
internal fun gitClone(repoUrl: String, branch: String = "", dir: String) {
    gitClone(repoUrl, dir)
    if (branch.isNotEmpty()) {
        gitCheckout(branch, dir)
    }
}




object GitMsg {
    //成功
    const val CloningInto = "Cloning into"
    const val SwitchToANewBranch = "Switched to a new branch"

    //失败
    const val ALREADY_EXISTS = "already exists"
    const val ERROR = "ERROR"

    /**
     * 是否错误
     */
    fun String.isError(): Boolean {
        return contains(ALREADY_EXISTS) || contains(ERROR)
    }

    /**
     * 是否成功
     */
    fun String.isSuccessful(): Boolean {
        return contains(CloningInto) || contains(SwitchToANewBranch)
    }
}