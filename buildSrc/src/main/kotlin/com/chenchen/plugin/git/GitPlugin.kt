package com.chenchen.plugin.git

import com.chenchen.plugin.meta.GitScope
import com.chenchen.plugin.meta.GitStore
import kotlinx.coroutines.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

/**
 *
 * 示例用法
 * ```
 * gitClone {
 *  gitStore("https://gitee.com") {
 *      gitProject(
 *          "jaso_chen.com/camera-study",
 *          "master",
 *          buildDir.absolutePath + "/testDir",
 *          "CameraStudy")
 *      gitProject(
 *          "jaso_chen.com/camera-study",
 *          "testBranch",
 *          buildDir.absolutePath + "/testDir",
 *          "CameraStudy")
 *  }
 * }
 * ```
 */
class GitPlugin : Plugin<Project> {

    val coroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO +
            CoroutineExceptionHandler { _, throwable ->
                println(throwable.message)
            })

    override fun apply(target: Project) {
        //配置git扩展
        target.extensions.create("gitClone", GitScope::class.java)
        //定义gitClone任务
        target.tasks.create("gitClone").apply {
            group = "git"
            description = "用于管理多仓库初始化、切换分支"
            doLast(this@GitPlugin::gitCloneTask)
        }
    }

    /**
     * 执行gitClone程序
     */
    private fun gitCloneTask(task: Task) = runBlocking {
        coroutineScope.launch {
            val gitScope = task.project.extensions.getByName("gitClone") as GitScope
            for (store in gitScope.stores) {
                cloneStoreTask(store)
            }
        }.join()
    }

    /**
     * 按模块拉取仓库
     */
    private fun CoroutineScope.cloneStoreTask(store: GitStore) {
        for (project in store.projects) {
            async {
                gitClone("${store.host}/${project.url}", project.branch,
                    "${project.saveDir.replace("\\", "/")}/${project.rename}"
                )
            }
        }
    }
}