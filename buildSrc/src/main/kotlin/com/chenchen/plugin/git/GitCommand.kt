package com.chenchen.plugin.git


/**
 * clone仓库
 */
internal fun gitClone(repoUrl: String, dir: String) {
    "git clone $repoUrl $dir".exeCommand()
}

/**
 * 切换分支
 */
internal fun gitCheckout(branch: String, dir: String) {
    "git checkout -b $branch origin/$branch".exeCommand(dir)
}