package com.chenchen.plugin.meta

data class GitStore(val host: String) {
    internal val projects = arrayListOf<GitProject>()
}