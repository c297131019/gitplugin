package com.chenchen.plugin.meta

data class GitProject(
    //项目链接
    val url: String,
    //项目分支，以后切换分支大概就是修改branch
    val branch: String = "",
    //存放路径
    val saveDir: String,
    //是否需要重命名文件夹
    val rename: String = ""
)