使用方式：

1.在任意一个`build.gradle.kts`文件中添加插件
```
plugins {
    id 'GitPlugin'
}
```
> 为什么是`build.gradle.kts`? 因为插件是用`kotlin`写的，里面包含了一些扩展方法，在`groovy`中不会使用。

2.在该`build.gradle.kts`文件中编写需要拉取的项目
```
//最好导入一下类，编辑器不一定能识别
import com.chenchen.plugin.meta.*

//gitClone是插件里定义的
gitClone {
 //很多项目的远程仓库不止一个，这里可以设置仓库的域名
 gitStore("https://gitee.com") {
     //配置仓库中的项目
     gitProject(
        //项目链接
        url = "jaso_chen.com/camera-study",
        //项目分支，以后切换分支大概就是修改branch
        branch = "master",
        //存放路径
        saveDir = buildDir.absolutePath + "/testDir",
        //是否需要重命名文件夹
        rename = "CameraStudy")
     gitProject(
        url = "jaso_chen.com/camera-study",
        branch = "testBranch",
        saveDir = buildDir.absolutePath + "/testDir",
        rename = "CameraStudyTest")
 }
}
```

3.编写好需要拉取的项目后点击`sync project with Gradle Files`进行同步

4.同步好之后可以在右边的`Gradle`列表中找到对应的任务，具体的任务在编写的`build.gradle.kts`模块名下的`tasks/git/gitClone`

5.可以在控制台执行命令，或者双击点击`Gradle`任务列表的命令
```
在控制台输入(总有一个能用的)：
gradle gitClone
or
gradlew gitClone
or
./gradlew gitClone
```

6.在`app/build.gradle.kts`中有实例代码，可以尝试运行